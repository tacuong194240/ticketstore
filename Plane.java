import org.json.JSONObject;

public class Plane {
    int flyTime;
    String flyPlace;
    int landTime;
    String landPlace;

    public Plane(int flyTime, String flyPlace, int landTime, String landPlace){
        this.flyTime   = flyTime;
        this.flyPlace  = flyPlace;
        this.landTime  = landTime;
        this.landPlace = landPlace;
    }

    public Plane(String jsonString){
        var json = new JSONObject(jsonString);
        this.flyTime     = json.getInt("flyTime");
        this.flyPlace    = json.getString("flyPlace");
        this.landTime    = json.getInt("landTime");
        this.landPlace   = json.getString("landPlace");
    }

    protected JSONObject toJsonObject(){
        var json = new JSONObject();
        json.put("flyTime", this.flyTime);
        json.put("flyPlace", this.flyPlace);
        json.put("landTime", this.landTime);
        json.put("landPlace", this.landPlace);
        return json;
    }
}
