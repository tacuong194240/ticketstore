import java.util.ArrayList;
import java.util.Collections;

import javax.swing.*;

public class Main {
    public static void main(String[] args){
        DataBase.seed();
        buildFrame();
    }

    static void buildFrame(){
        // input from user
        JTextField input = new JTextField(20);

        // Buttons
        JButton enterButton = new JButton("Enter");
        JButton ticketButton = new JButton("Show Tickets");
        JButton planeButton = new JButton("Show Planes");

        // input and button container
        JPanel inputpanel = new JPanel();
        inputpanel.add(input);
        inputpanel.add(enterButton);
        inputpanel.add(ticketButton);
        inputpanel.add(planeButton);

        // text area
        JTextArea output = new JTextArea(5, 60);
        output.setEditable(false);
        output.setText("output goes here");

        JPanel showBoard = new JPanel();
        showBoard.setLayout(new BoxLayout(showBoard, BoxLayout.Y_AXIS));

        // container of containers
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS)); // make layout vertical
        panel.add(output);
        panel.add(inputpanel);
        panel.add(showBoard);

        JScrollPane scroll = new JScrollPane();
        scroll.setViewportView(panel);

        // window
        JFrame frame = new JFrame("Test");
        frame.setSize(600, 600);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // make program stop when close window
        frame.getContentPane().add(scroll);
        frame.setVisible(true);

        // button click action
        enterButton.addActionListener((e) -> {
            var content = input.getText();
            output.setText(content);
        });

        String[] ticketTitles = { "id", "price", "plane id", "type", "soldTime", };
        String[] planeTitles = { "id", "landPlace", "landTime", "flyTime" ,"flyPlace", };
        ticketButton.addActionListener((e) -> showTable(ticketTitles, "tickets", showBoard, frame));
        planeButton.addActionListener((e) -> showTable(planeTitles, "planes", showBoard, frame));
    }

    static void refresh(JFrame frame){
        frame.invalidate();
        frame.validate();
        frame.repaint();
    }

    static void newTableData(String content,Object parent){
        JTextField td = new JTextField(10);
            td.setText(content);
            td.setEditable(false);
            ((JPanel) parent).add(td);
    }

    static void showTable(String[] titles, String tableName, JPanel showBoard, JFrame frame){
        var tickets  = DataBase.getTable(tableName);
        JPanel thead = new JPanel();
        for(var title : titles){
            newTableData(title, thead);
        }
        JPanel tbody = new JPanel();
        tbody.setLayout(new BoxLayout(tbody, BoxLayout.Y_AXIS));

        ArrayList<Integer> sortedIds = new ArrayList<>();
        for(var name : tickets.names()){
            sortedIds.add(Integer.parseInt(name.toString()));
        }
        Collections.sort(sortedIds);
        
        for(var name : sortedIds){
            var ticket = tickets.getJSONObject(name.toString());
            JPanel tr = new JPanel();
            newTableData(name.toString(), tr);
            for(var prop : ticket.names()){
                newTableData(String.valueOf(ticket.get(prop.toString())), tr);
            }
            tbody.add(tr);
        }
        showBoard.removeAll();
        showBoard.add(thead);
        showBoard.add(tbody);
        refresh(frame);
    }
}
