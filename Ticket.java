import org.json.JSONObject;

public class Ticket {
    protected int type = 1; // 1 for homeland, 0 for global
    protected int price;
    protected int soldTime = 0; // TODO should change all date to string?
    protected int planeId;

    public Ticket(int price, int planeId){
        this.price   = price;
        this.planeId = planeId;
    }

    public Ticket(int price, int flyTime, String flyPlace, int landTime, String landPlace){
        var plane = new Plane(flyTime, flyPlace, landTime, landPlace);
        var planeId = DataBase.create(plane.toJsonObject(), "planes"); // TODO check if plane exists

        this.price   = price;
        this.planeId = planeId;
    }

    public Ticket(String jsonString){
        var json = new JSONObject(jsonString);
        this.type     = json.getInt("type");
        this.price    = json.getInt("price");
        this.soldTime = json.getInt("soldTime");
        this.planeId  = json.getInt("planeId");
    }

    protected JSONObject toJsonObject(){
        var json = new JSONObject();
        json.put("type", this.type);
        json.put("price", this.price);
        json.put("soldTime", this.soldTime);
        json.put("planeId", this.planeId);
        return json;
    }

    protected void makeGlobal(){
        this.type = 0;
    }

    protected boolean isSold(){
        return this.soldTime != 0;
    }

    protected Plane getPlane(){
        var jsonString = DataBase.read(this.planeId, "planes");
        return (jsonString.length() >= 2) ? new Plane(jsonString) : null;
    }
}